//
//  PlateRecognitionResultsViewController.swift
//  CustomCamera
//
//  Created by Private on 8/30/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class PlateRecognitionResultsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var myData: [String] = ["AA 3454 ВС", "GU 10 TAG", "BK 5764 CA"]
    var matchedData: [String] = ["GU 10 TAG"]
    var owner: [String] = ["James Brown"]

    @IBOutlet weak var table1: UITableView!
    @IBOutlet weak var table2: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (tableView == table1) {
           return myData.count
        }
        else {
            return matchedData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == table1) {
            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            cell.textLabel?.text = myData[indexPath.row]
            cell.textLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            return cell
        }
        else {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2") as! PlateTableViewCell
            cell2.plateNumber.text = matchedData[indexPath.row]
            cell2.OwnerName.text = owner[indexPath.row]
            cell2.OwnerName.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell2.plateNumber.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            return cell2
        }
    }
}
