//
//  UploadDetailsViewController.swift
//  CustomCamera
//
//  Created by Private on 9/7/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import Alamofire
import SwiftSocket
import SocketIO
import UIKit

class UploadDetailsViewController: ViewController, UITableViewDataSource, UITableViewDelegate {
    
    var name = ["Piece 1", "Piece 2", "Piece 3", "Piece 4"]
    
    var receivedImage: UIImage!
    
    @IBOutlet weak var picture: UIImageView!
    var photo: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
        
        
        
//       socket.on("getPhoto") { data,ack in
//            if let buffer = data[0] as? String {
//                let decodedData = NSData(base64Encoded: buffer, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
//                let img = UIImage(data: decodedData! as Data)
//            }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! UploadDetailsCell
        cell.label?.text = name[indexPath.row]
        cell.picture?.image = receivedImage
        cell.label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return cell
    }
}
