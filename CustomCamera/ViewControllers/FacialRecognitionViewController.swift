//
//  FacialRecognitionViewController.swift
//  CustomCamera
//
//  Created by Private on 8/29/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class FacialRecognitionViewController: UIViewController {

    @IBAction func TakePhotoTapped(_ sender: Any) {
        let landingPage = self.storyboard?.instantiateViewController(withIdentifier:"ViewController") as! ViewController
        self.present(landingPage, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
