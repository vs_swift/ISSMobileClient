//
//  FacesTableViewCell.swift
//  CustomCamera
//
//  Created by Private on 8/31/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class FacesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var facesImage: UIImageView!
    
    @IBOutlet weak var facesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
