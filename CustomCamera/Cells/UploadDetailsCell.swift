//
//  UploadDetailsCell.swift
//  CustomCamera
//
//  Created by Private on 9/7/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class UploadDetailsCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
