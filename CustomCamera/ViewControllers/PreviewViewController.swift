import Alamofire
import AlamofireImage
import SwiftyJSON
import SocketIO
import UIKit
import SwiftSocket

class PreviewViewController: UIViewController {
    
    var image: UIImage!
    var receivedImage: UIImage!
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func uploadButtonTapped(_ sender: Any) {
        connect()
    }
    @IBOutlet weak var photo: UIImageView!
    
    @IBAction func PlateButtonTapped(_ sender: Any) {
    }

    @IBAction func FacialButtonTapped(_ sender: Any) {
    }
    
    var socket:SocketIOClient!
    
    var imageData = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photo.image = self.image
    }
    
    func connect() {
        let client = TCPClient(address: "192.168.1.7", port: 54000)
        switch client.connect(timeout: 10) {
        case .success:
            let data = UIImageJPEGRepresentation(image, 0.7)
            let base64EnCodedStr:String = (data?.base64EncodedString(options: .lineLength64Characters))!
            switch client.send(string:base64EnCodedStr) {
            case .success:
                guard let data = client.read(999000) else { return }
                if let response = String(bytes: data, encoding:.utf8) {
                    let decodedData:NSData = NSData(base64Encoded: response, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
                }
            case .failure(let error):
                print(error)
            }
        case .failure(let error):
            print("error2")
        }
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "test" {
//            let uploadDetailsVC = segue.destination as! UploadViewController
//            uploadDetailsVC.receivedImage = self.receivedImage
//        }
//    }
}
    

    

    
            
    
        
        
    

    
    


