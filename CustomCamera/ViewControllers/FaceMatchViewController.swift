//
//  FaceMatchViewController.swift
//  CustomCamera
//
//  Created by Private on 8/31/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class FaceMatchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var mainPhoto: UIImageView!
    
    var noMatchesFound = "I am sorry, no matches found =("
    var matches3 = "Julia Soares"
    var matches4 = "Warner Vogels"
    
    @IBOutlet weak var label: UILabel!
    var name = ""
    var face = UIImage()
    var matchedFemaleFaces = #imageLiteral(resourceName: "Screen Shot 2018-09-01 at 5.42.26 PM.png")
    var matchedMaleFaces = #imageLiteral(resourceName: "Werner-Vogels-headshot-1-HI-RES-e1524071012713.jpg")
    var NoMatchesRobot = #imageLiteral(resourceName: "robot3.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "\(name) is matching the following people in our database:"
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mainPhoto.image = face
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! FaceDetailsTableViewCell
        if (name == "Face 1") {
            cell.faceDetailsLabel.text = matches3
            cell.faceDetailsImage.image = matchedFemaleFaces
        } else if (name == "Face 2") {
            cell.faceDetailsLabel.text = matches4
            cell.faceDetailsImage.image = matchedMaleFaces
        } else if (name == "Face 3") {
            cell.faceDetailsLabel.text = noMatchesFound
            cell.faceDetailsImage.image = NoMatchesRobot
        }
        cell.faceDetailsLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return cell
    }
}
