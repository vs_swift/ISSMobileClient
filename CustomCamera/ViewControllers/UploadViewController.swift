//
//  UploadViewController.swift
//  CustomCamera
//
//  Created by Private on 9/7/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//
import UIKit

class UploadViewController: ViewController {
    
    var receivedImage: UIImage!

    @IBOutlet weak var testImage: UIImageView!
    @IBAction func GetResultTapped(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        testImage.image = receivedImage
    }
}
