//
//  PlateTableViewCell.swift
//  CustomCamera
//
//  Created by Private on 9/1/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class PlateTableViewCell: UITableViewCell {

    @IBOutlet weak var plateNumber: UILabel!
    @IBOutlet weak var OwnerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
