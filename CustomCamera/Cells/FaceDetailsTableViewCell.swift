//
//  FaceDetailsTableViewCell.swift
//  CustomCamera
//
//  Created by Private on 8/31/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class FaceDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var faceDetailsImage: UIImageView!
    @IBOutlet weak var faceDetailsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
