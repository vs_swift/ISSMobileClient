//
//  FacialRecognitionResultViewController.swift
//  CustomCamera
//
//  Created by Private on 8/31/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class FacialRecognitionResultViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    var name = ["Face 1", "Face 2", "Face 3"]
    
    var photos = [#imageLiteral(resourceName: "female.png"),#imageLiteral(resourceName: "WonCamera.png"),#imageLiteral(resourceName: "face2.png")]
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! FacesTableViewCell
        cell.facesLabel.text = name[indexPath.row]
        cell.facesImage.image = photos[indexPath.row]
        cell.facesLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FaceMatchViewController") as? FaceMatchViewController
        vc?.name = name[indexPath.row]
        vc?.face = photos[indexPath.row]
        self.present(vc!, animated: true, completion: nil)
    }
}
